#ifndef UBSPOTPLUGIN_H
#define UBSPOTPLUGIN_H

#include <QObject>

class Ubspotplugin: public QObject {
    Q_OBJECT

public:
    Ubspotplugin();
    ~Ubspotplugin() = default;

    Q_INVOKABLE void speak();
};

#endif
