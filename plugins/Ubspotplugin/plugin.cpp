#include <QtQml>
#include <QtQml/QQmlContext>

#include "plugin.h"
#include "ubspotplugin.h"

void UbspotpluginPlugin::registerTypes(const char *uri) {
    //@uri Ubspotplugin
    qmlRegisterSingletonType<Ubspotplugin>(uri, 1, 0, "Ubspotplugin", [](QQmlEngine*, QJSEngine*) -> QObject* { return new Ubspotplugin; });
}
