import QtQuick 2.4
import QtQuick.Layouts 1.1
import QtQuick.LocalStorage 2.0
import Ubuntu.Components 1.3
import Ubuntu.Connectivity 1.0
import Ubspotplugin 1.0
import Ubuntu.Web 0.2

import "js/scripts.js" as Scripts
import "js/db.js" as LocalDB
import "ui" as Ui

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'ubspot.mungorae'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    AdaptivePageLayout {
        id: pageLayout
        anchors.fill: parent
        primaryPage: homePage

        // Pages
        Ui.Home {
            id: homePage
        }
    }

    Component.onCompleted: {
        Scripts.get_access_token(function callback(access_token) {
            if (access_token == null) {
                pageLayout.primaryPageSource = Qt.resolvedUrl("ui/Login.qml");
            }
        });
    }
}
