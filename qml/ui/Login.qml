import QtQuick 2.4
import QtQuick.LocalStorage 2.0

import Ubuntu.Components 1.3
import Ubuntu.Web 0.2

Page {
    id: loginPage
    anchors.fill: parent

    header: PageHeader {
        id: header
        title: i18n.tr('UBSpot')
    }

    WebView {
        id: webView

        // the redirect_uri can be any site
        property string redirect_uri: "https://mungorae.me.uk/ubspot"
        url: "https://accounts.spotify.com/authorize?client_id=19af25fae2e54efb907b85a9b5878aa3&response_type=token&redirect_uri=" + encodeURIComponent(redirect_uri)

        onUrlChanged: {
            console.log(">>> Loading Url: " + url.toString())
            if (url.toString().includes(redirect_uri)) {
                console.error(">>> Includes: " + redirect_uri)
                Scripts.save_access_token(url.toString())
            }
        }

        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            top: loginPage.header.bottom
        }

        incognito: true
        preferences.localStorageEnabled: true
        preferences.allowFileAccessFromFileUrls: true
        preferences.allowUniversalAccessFromFileUrls: true
        preferences.appCacheEnabled: true
        preferences.javascriptCanAccessClipboard: true
    }
}
