import QtQuick 2.4
import Ubuntu.Components 1.3
import QtQuick.LocalStorage 2.0

import "../js/db.js" as LocalDB
import "../js/scripts.js" as Scripts

Page {
    header: PageHeader {
        id: pageHeader
        title: "Home"
    }

    Column {
        anchors.fill: parent
        spacing: units.gu(3)

        Label {
            id: testKeyLabel
            color: "steelblue"

            text: "No access token"
            font.pointSize: 12

            Component.onCompleted: {
                Scripts.get_access_token(function callback(access_token) {
                    if (access_token) {
                        text = access_token;
                    }
                });
            }
        }
    }

    function downloadSongs(access_token) {
        var request = new XMLHttpRequest;
        request.open("GET", "https://api.spotify.com/v1/me/tracks");

        request.setRequestHeader("Authorization", "Bearer " + access_token);

        request.onreadystatechange = function () {
            if (request.readyState == XMLHttpRequest.DONE) {
                if (request.status !== 0) {
                    console.log("Response: " + request.responseText);
                    var results = JSON.parse(request.responseText);
                    if (results.error) {
                        if (results.error.status == 401) {
                            pageLayout.removePages(pageLayout.primaryPage);
                            pageLayout.primaryPageSource = Qt.resolvedUrl("Login.qml")
                        }
                    }
                }
            }
        }
        request.send()
    }

    Component.onCompleted: {
        Scripts.get_access_token(function callback(access_token) {
            if (access_token) {
                console.log("Got access token: " + access_token);
                downloadSongs(access_token);
            }
        });
    }
}
