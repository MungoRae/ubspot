function save_access_token(redirect_uri) {
    var result = redirect_uri.split("#")[1];
    var keyvalues = result.split("&");
    var dict = {};
    keyvalues.forEach(function(it) {
        var keyvalue = it.split("=");
        dict[keyvalue[0]] = keyvalue[1];
    });
    var db = LocalDB.init();
    db.transaction(function(tx) {
        var rs = tx.executeSql("INSERT OR REPLACE INTO settings VALUES(?,?);", ["access_token", dict["access_token"]]);
        if (rs.rowsAffected == 0) {
            throw "Error updating access_token";
        } else {
            console.log("access_token updated to " + dict["access_token"])
        }
    });
}

function get_access_token(callback) {
    var db = LocalDB.init();
    db.transaction(function(tx) {
        var access_token = null;
        var rs = tx.executeSql("SELECT * FROM settings");
        for (var i = 0; i < rs.rows.length; i++) {
            var row = rs.rows.item(i);
            if (row.key == 'access_token') {
                access_token = row.value;
                break;
            }
        }

        callback(access_token);
    });
}
