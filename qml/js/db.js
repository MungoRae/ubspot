// DB
function init() {
    var db = LocalStorage.openDatabaseSync("ubspot", "1.0", "Database for UBSpot", "1000000");

    db.transaction(function(tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS settings(key TEXT UNIQUE, value TEXT)');
    });

    return db;
}
